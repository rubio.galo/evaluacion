# Evaluaciones de habilidades

Se necesita almacenar la data que se recibe por el API http://172.24.31.11/prueba.php en una base de datos, para posteriormente motrarla en una pagina.

## Condiciones

1. Se debe levantar el servidor mariadb utilizando preferiblemente docker redireccionando el puerto 3306 al 2023. (-p [host_port]:[container_port])
2. Crear la base de datos con la siguiente estructura.

![plot](./Captura.PNG)

3. Los nombre almacenados no deben contener espacios al inicio o final y deben estar en minúsculas. (Ejm dato = "   NomBre  Maquina " se debe guardar "nombre maquina").
3. Para consumir el API preferiblemente utilizar Python (PHP puede ser válido).
4. El la Web se debe mostrar la siguiente tabla.

| Host | IP | Promedio CPU |Promedio RAM |
| -- | -- | -- |-- |
| nombre | ip_add | 0.00 |0.00 |
| ... | ... | ... |... |

5. Una vez almacenados los datos realizar un respaldo sql de la base de datos.
6. Subir subir el codigo y el respaldo al repositorio en una nueva rama con su nombre de usuario.